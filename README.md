1)
var declaration is oldest declaration method.
var variables can be re-declared and updated.But there's a weakness that comes with var.
For instance,you have assigned name variable using var,and then if you want to use this variable inside of "if" condition,then you must redefine it.
This can be a problem.This is why the let and const is necessary.

Let is preferred for variable declaration now.
It's no surprise as it comes as an improvement to the var declarations.It also solves this problem that was raised in the last subheading.
Let can be updated but not re-declared.


Variables declared with the const maintain constant values.const declarations share some similarities with let declarations.
const cannot be updated or re-declared


2)there's a weakness that comes with var.
While this is not a problem if you knowingly want "variable" to be redefined, 
it becomes a problem when you do not realize that a variable greeter has already been defined before.
If you have use greeter in other parts of your code,
 you might be surprised at the output you might get. This might cause a lot of bugs in your code.